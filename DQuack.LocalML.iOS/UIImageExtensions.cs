﻿using CoreGraphics;
using CoreVideo;
using Foundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UIKit;

namespace DQuack.LocalML.iOS
{
    public static class UIImageExtensions
    {
        public static CVPixelBuffer ToCVPixelBuffer(this UIImage image, CGSize size)
        {
            var attrs = new CVPixelBufferAttributes
            {
                CGImageCompatibility = true,
                CGBitmapContextCompatibility = true,
            };
            CGImage cgImage = image.Scale(size).CGImage;

            // TODO: I genuinely have no idea what's happening here!
            var pb = new CVPixelBuffer(cgImage.Width, cgImage.Height, CVPixelFormatType.CV32ARGB, attrs);
            pb.Lock(CVPixelBufferLock.None);
            IntPtr pData = pb.BaseAddress;
            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();
            var context = new CGBitmapContext(pData, cgImage.Width, cgImage.Height, 8, pb.BytesPerRow, colorSpace, CGImageAlphaInfo.NoneSkipFirst);
            context.TranslateCTM(0, cgImage.Height);
            context.ScaleCTM(1.0f, -1.0f);
            UIGraphics.PushContext(context);
            image.Draw(new CGRect(0, 0, cgImage.Width, cgImage.Height));
            UIGraphics.PopContext();
            pb.Unlock(CVPixelBufferLock.None);

            return pb;
        }

        public static async Task<UIImage> ToUIImage(this Stream stream)
        {
            var bytes = new byte[stream.Length];
            await stream.ReadAsync(bytes, 0, bytes.Length);

            return UIImage.LoadFromData(NSData.FromArray(bytes));
        }
    }
}
