﻿using DQuack.LocalML.Abstractions;
using System;

namespace DQuack.LocalML
{
    public static class CrossMLInterface
    {
        private static Lazy<IMLInterface> implementation = new Lazy<IMLInterface>(() => CreateMLInterface(), System.Threading.LazyThreadSafetyMode.PublicationOnly);

        public static bool IsSupported => implementation.Value != null;
        internal static Exception NotImplementedInReferenceAssembly() =>
            new NotImplementedException("This functionality is not implemented in the portable version of this assembly.  You should reference the NuGet package from your main application project in order to reference the platform-specific implementation.");

        public static IMLInterface Current
        {
            get
            {
                IMLInterface ret = implementation.Value;
                if (ret == null)
                    { throw NotImplementedInReferenceAssembly(); }
                return ret;
            }
        }

        private static IMLInterface CreateMLInterface()
        {
#if NETSTANDARD2_0
            return null;
#else
            return new MLInterface();
#endif
        }
    }
}
